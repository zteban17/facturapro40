<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="application/pdf; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte</title>
</head>

<body>
    <table width="100%">
        <tr>
            <td width="100%" class="text-center">
                <span class="title">Reporte Deudas Pendientes</span> <br>
            </td>
        </tr>
    </table>
    <hr />
    <div style="margin-top:5px; margin-bottom:20px;">
        <table>
            <tr>
                <td width="15%">
                    <strong>Empresa:</strong>
                </td>
                <td width="85%">
                    </strong>{{ $company->name }}</p>
                </td>
            </tr>
            <tr>
                <td width="15%">
                    <strong>RUC:</strong>
                </td>
                <td width="85%">
                    </strong>{{ $company->number }}</p>
                </td>
            </tr>
        </table>
    </div>
    @if (!empty($reports))
        @php
            $total = 0;
        @endphp
        @foreach ($reports as $cliente => $sale_notes)
            <div class="sub-title">Cliente: {{ $cliente }}</div>
            <div>
                <table class="full-width mt-10 mb-10">
                    <thead class="">
                        <tr>
                            <th class="border-top-bottom desc-9 text-left" width="10%">Fecha Emisión</th>
                            <th class="border-top-bottom desc-9 text-left" width="10%">Nota de Venta</th>
                            <th class="border-top-bottom desc-9 text-left" width="10%">Estado</th>
                            <th class="border-top-bottom desc-9 text-left" width="10%">Moneda</th>
                            <th class="border-top-bottom desc-9 text-left" width="10%">T.Exportación</th>
                            <th class="border-top-bottom desc-9 text-left" width="10%">T.Inafecta</th>
                            <th class="border-top-bottom desc-9 text-left" width="10%">T.Exonerado</th>
                            <th class="border-top-bottom desc-9 text-left" width="10%">T.Gravado</th>
                            <th class="border-top-bottom desc-9 text-left" width="10%">T.Igv</th>
                            <th class="border-top-bottom desc-9 text-left" width="10%">Total</th>
                        </tr>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $subtotal = 0;
                            $total_taxed = 0;
                        @endphp
                        @foreach ($sale_notes as $value)
                            @php
                                $subtotal += $value->total;
                                $total_taxed += $value->total_taxed;
                                $total += $value->total;
                            @endphp
                            <tr>
                                <td class="desc-9 text-left font-none">
                                    {{ $value->date_of_issue->format('Y-m-d') }}
                                </td>
                                <td class="desc-9 text-left font-none">
                                    {{ $value->identifier }} L/min</td>
                                <td class="desc-9 text-left font-none">
                                    {{ $value->state_type->description }}
                                </td>
                                <td class="desc-9 text-left font-none">
                                    {{ $value->currency_type_id }}</td>
                                <td class="desc-9 text-left font-none">
                                    {{ $value->currency_type->symbol . ' ' . $value->total_exportation }}
                                </td>
                                <td class="desc-9 text-left font-none">
                                    {{ $value->currency_type->symbol . ' ' . $value->total_unaffected }}
                                </td>
                                <td class="desc-9 text-left font-none">
                                    {{ $value->currency_type->symbol . ' ' . $value->total_exonerated }}
                                </td>
                                <td class="desc-9 text-left font-none">
                                    {{ $value->currency_type->symbol . ' ' . $value->total_taxed }}
                                </td>
                                <td class="desc-9 text-left font-none">
                                    {{ $value->currency_type->symbol . ' ' . $value->total_igv }}
                                </td>
                                <td class="desc-9 text-left font-none">
                                    {{ $value->currency_type->symbol . ' ' . $value->total }}
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <th class="border-bottom desc-9 text-left" colspan="9"><b>TOTAL</b>
                            </th>
                            <th class="border-bottom desc-9 text-left">
                                {{ $value->currency_type->symbol . ' ' . round($subtotal, 3) }}
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
        @endforeach
        <table class="full-width">
            <tr>
                <td class="text-left">
                    <h4>RESUMEN DE DEUDAS PENDIENTES</h4>
                </td>
            </tr>¿
        </table>
        <table class="full-width mb-20">

            <tr>
                <td width="90%">
                    <p class="desc">TOTAL:</p>
                </td>
                <td width="10%">
                    <p class="desc"><b>{{ $value->currency_type->symbol . ' ' . round($total, 3) }}</p>
                </td>
            </tr>

            <tr>
                <td>
                    <p class="desc">Observaciones:
                        <br>
                        <br>
                        <br>
                        <br>
                    </p>
                </td>
                <td>
                    <p class="desc"></p>
                </td>
            </tr>
        </table>
        <div class="mb-20 mt-20"></div>
        <table class="full-width mt-20">
            <tr>
                <td width="" class="text-center pt-3">
                    <p class="desc">.................................................</p>
                    <p class="desc">Firma Encargado Turno</p>
                </td>
                <td width="" class="text-center pt-3">
                    <p class="desc">.................................................</p>
                    <p class="desc">Firma Recepción</p>
                </td>
            </tr>
        </table>

    @else
        <div class="callout callout-info">
            <p>No se encontraron registros.</p>
        </div>
    @endif
</body>

</html>
