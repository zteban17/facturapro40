<?php

namespace Modules\Report\Http\Controllers;

use App\Classes\MPDFClass;
use App\Exports\DebtsExport;
use Illuminate\Http\Request;
use Modules\Report\Traits\ReportTrait;
use Illuminate\Http\Response;
use App\Models\Tenant\SaleNote;
use App\Models\Tenant\Catalogs\DocumentType;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use App\Models\Tenant\Document;
use App\Models\Tenant\Establishment;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Company;
use App\Http\Resources\Tenant\SaleNoteCollection;

class ReportDebtsController extends Controller
{
    use ReportTrait;

    public function filter()
    {

        $document_types = DocumentType::whereIn('id', ['01', '03', '07', '08'])->get();

        $persons = $this->getPersons('customers');
        $sellers = $this->getSellers();

        $establishments = Establishment::all()->transform(function ($row) {
            return [
                'id' => $row->id,
                'name' => $row->description
            ];
        });

        return compact('document_types', 'establishments', 'persons', 'sellers');
    }
    public function index()
    {
        return view('report::debts.index');
    }

    public function records(Request $request)
    {
        $records = $this->getRecords($request->all());

        return new SaleNoteCollection($records->paginate(config('tenant.items_per_page')));
    }
    public function pdf(Request $request) {
        set_time_limit (1800); // Maximo 30 minutos
        $company = Company::first();
        $establishment = ($request->establishment_id) ? Establishment::findOrFail($request->establishment_id) : auth()->user()->establishment;
        $reports = $this->getRecords($request->all())->get();
        $filters = $request->all();

        $reports = $reports->groupBy(
            function($item, $key) {
                return $item['customer']->name;
            }
        );

        $pdf = new MPDFClass(
            [
                'margin_top'   => 10,
                'margin_bottom' => 20,
                'margin_header'        => 5,
                'margin_footer'        => 5,
            ]
        );
        $user = auth()->user()->name;
        $pdf->getMpdf()->SetFooter('Generado el: {DATE d-m-Y h:i A}|' . $user . '|Página {PAGENO} de {nbpg}');
        $pdf->loadViewHeavy('tenant.reports.debts.report_pdf', compact("reports", "company"));

        $filename = 'Reporte_Deudas_Pendientes'.date('YmdHis');
        return $pdf->stream($filename.'.pdf');
    }




    public function excel(Request $request) {

        $company = Company::first();
        $establishment = ($request->establishment_id) ? Establishment::findOrFail($request->establishment_id) : auth()->user()->establishment;

        $records = $this->getRecords($request->all())->get();
        $filters = $request->all();


        return (new DebtsExport)
                ->records($records)
                ->company($company)
                ->download('ReporteNotaVent'.Carbon::now().'.xlsx');

    }

    /**
     * @param $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function getRecords($request)
    {
        $records = SaleNote::whereTypeUser();

        $records->where('total_canceled', 0);

        $records->orderBy('id','DESC');

        return $records;
    }
}
